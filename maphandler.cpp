/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "maphandler.h"

#include	"fedmap.h"
#include "location.h"
#include "tesla.h"
#include "misc.h"
#include "star.h"


const std::string	MapHandler::el_names[] =
{ "fed2-map", "location", "exits", "events", "name", "desc", "" };
const std::string MapHandler::dir_names[] =
{ "n", "ne", "e", "se", "s", "sw", "w", "nw", "up", "down", "in", "out", "" };
const std::string MapHandler::ev_names[] = { "enter", "no-exit", "in-room", "" };

const int	MapHandler::NO_ELEMENT = 99999;


MapHandler::~MapHandler()
{
	// Make sure destructors are called for the whole heirarchy
}


bool	MapHandler::characters(const QString& ch)
{
	if ((text_needed != NO_TEXT) && (current != 0))
		buffer += ch.toStdString();
	return(true);
}

bool	MapHandler::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	std::ostringstream	buff;
	switch (FindElement(qName.toStdString()))
	{
	case 0:	NsTesla::star->AddMap(fed_map);
		buff << fed_map->FileRoot() << ".loc";
		Tesla::Instance()->DisplayFileName(buff.str());
		break;
	case 1:	if (current != 0)
		fed_map->AddLocation(current);
		current = 0;
		break;
	case 4:	if (current != 0)
		current->AddName(buffer);
		text_needed = NO_TEXT;
		break;
	case 5:	if (current != 0)
		current->AddDesc(buffer);
		text_needed = NO_TEXT;
		break;
	}
	return(true);
}

int	MapHandler::FindElement(const std::string& elem)
{
	int which;
	for (which = 0; el_names[which] != ""; which++)
	{
		if (elem == el_names[which])
			return(which);
	}
	return(NO_ELEMENT);
}

bool	MapHandler::startElement(const QString& namespaceURI, const QString& localName,
	const QString& qName, const QXmlAttributes& atts)
{
	switch (FindElement(qName.toStdString()))
	{
	case 0:	StartMap(atts);				break;
	case 1:	StartLocation(atts);			break;
	case 2:	StartExits(atts);				break;
	case 3:	StartEvents(atts);			break;
	case 4:	buffer = "";
		text_needed = NAME_ELEM;	break;
	case 5:	buffer = "";
		text_needed = DESC_ELEM;	break;

	default:										break;	/********* report error *********/
	}
	return(true);
}

void	MapHandler::StartEvents(const QXmlAttributes& attribs)
{
	if (current != 0)
	{
		QString	ev_text;
		for (int count = 0; ev_names[count] != ""; ++count)
		{
			ev_text = attribs.value(QString::fromStdString(dir_names[count]));
			if (ev_text != "")
				current->AddEvent(count, ev_text.toStdString());
		}
	}
}

void	MapHandler::StartExits(const QXmlAttributes& attribs)
{
	if (current != 0)
	{
		QString	dest;
		for (int count = 0; dir_names[count] != ""; ++count)
		{
			dest = attribs.value(QString::fromStdString(dir_names[count]));
			if (dest == "")
				current->AddExit(count, Location::NO_EXIT);
			else
				current->AddExit(count, dest.toInt());
		}
		current->AddNoExit(attribs.value(QString::fromLatin1("no-exit")).toStdString());
	}
}

void	MapHandler::StartLocation(const QXmlAttributes& attribs)
{
	current = new Location(attribs.value(QString::fromLatin1("num")).toInt());
	std::string	flags(attribs.value(QString::fromLatin1("flags")).toStdString());
	if (flags != "")
		current->AddFlags(flags);
}

void	MapHandler::StartMap(const QXmlAttributes& attribs)
{
	fed_map->title = attribs.value(QString::fromLatin1("title")).toStdString();
	fed_map->launch_loc = attribs.value(QString::fromLatin1("from")).toStdString();
	fed_map->orbit_loc = attribs.value(QString::fromLatin1("to")).toStdString();
}


