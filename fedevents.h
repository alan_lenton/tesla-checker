/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef FEDEVENTS_H
#define FEDEVENTS_H

#include <map>
#include <string>

class Event;
class FedMap;
class	FedMessages;

typedef	std::map<std::string, Event *, std::less<std::string> >	EventIndex;

class	FedEvents
{
private:
	std::string	file_name;
	std::string	file_root;
	EventIndex	index;

	bool	TestIdFormat();
	bool	TestScripts(const std::string file_name, FedMap *fed_map, FedMessages *messages);

public:
	FedEvents(const std::string& f_name);
	~FedEvents();

	Event		*FindEvent(const std::string& which);
	const std::string&	FileRoot()		{ return(file_root); }
	unsigned	Size()							{ return(index.size()); }
	bool		Test(FedMap *fed_map, FedMessages *messages);
	void		AddEvent(Event *event);
};

#endif

