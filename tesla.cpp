/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "tesla.h"

#include <QClipboard>
#include <QDesktopServices>
#include <QFileDialog>
#include <QFontDialog>
#include <QPainter>
#include <QSettings>
#include <QString>
#include <QTextCursor>
#include <QUrl>

#include "loader.h"
#include "misc.h"
#include "star.h"

Tesla	*Tesla::instance = 0;

Tesla::Tesla(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags)
{
	version = "Version 2.01";
	std::ostringstream	buffer;
	buffer << "Tesla - Federation 2 File Checker - " << version.c_str();
	ui.setupUi(this);
	setWindowTitle(buffer.str().c_str());
	CreateActions();
	CreateMainMenu();
	CreateStatusBar();
	instance = this;
	NsTesla::loader = new Loader;
	NsTesla::star = new Star;

	picture = QImage(":/Tesla/Resources/planet.png");
	ui.ImageWidget->installEventFilter(this);
	clipboard = QApplication::clipboard();

	QCoreApplication::setOrganizationName("ibgames");
	QCoreApplication::setOrganizationDomain("ibgames.com");
	QCoreApplication::setApplicationName("Tesla");

	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "Tesla");
	QFont	font(settings.value("font-type", "verdana").toString(), settings.value("font-size", 9).toInt());
	ui.centralWidget->setFont(font);
	resize(settings.value("win-size", QSize(780, 540)).toSize());
	move(settings.value("win-pos", QPoint(20, 20)).toPoint());
	directory = settings.value("cur-dir", "./").toString().toStdString();

	status = NO_ERRORS;
}

Tesla::~Tesla()
{
	QSettings settings(QSettings::IniFormat,QSettings::UserScope,"ibgames", "Tesla");
	settings.setValue("font-type", ui.centralWidget->font().family());
	settings.setValue("font-size", ui.centralWidget->font().pointSize());
	settings.setValue("win-size", size());
	settings.setValue("win-pos", pos());
	settings.setValue("cur-dir", QString::fromStdString(directory));

	delete NsTesla::star;
	delete NsTesla::loader;
}


void	Tesla::CheckSystem()
{
	if (NsTesla::star->TestMap())
	{
		if (NsTesla::star->TestMessages())
			NsTesla::star->TestEvents();
	}
}

void	Tesla::ClearFileList()
{
	ui.FileListBox->clear();
	status = NO_ERRORS;
}
void	Tesla::ClearReport()
{
	ui.ReportEdit->clear();
	ui.ReportEdit->setFontWeight(QFont::Normal);
	ui.ReportEdit->setTextColor(Qt::black);
}

void	Tesla::CopyResults()
{
	clipboard->setText(ui.ReportEdit->textCursor().selectedText());
}

void	Tesla::CreateActions()
{
	// File|Load
	load_action = new QAction("&Load", this);
	load_action->setShortcut(tr("Ctrl+L"));
	load_action->setStatusTip("Load in all the files for a system");
	connect(load_action, SIGNAL(triggered()), this, SLOT(LoadFiles()));

	// File|Exit
	exit_action = new QAction("E&xit", this);
	exit_action->setShortcut(tr("Ctrl+X"));
	exit_action->setStatusTip("Exit Tesla");
	connect(exit_action, SIGNAL(triggered()), this, SLOT(close()));

	// Edit|Clear Display
	clear_action = new QAction("Clear &Report", this);
	clear_action->setShortcut(tr("Ctrl+D"));
	clear_action->setStatusTip("Clear all results from the report display");
	connect(clear_action, SIGNAL(triggered()), this, SLOT(ClearReport()));

	// Edit|Font Display
	font_action = new QAction("Change &Font", this);
	font_action->setShortcut(tr("Ctrl+F"));
	font_action->setStatusTip("Change the font used by Tesla");
	connect(font_action, SIGNAL(triggered()), this, SLOT(Font()));

	// Edit|Copy
	copy_action = new QAction("&Copy", this);
	copy_action->setShortcut(tr("Ctrl+C"));
	copy_action->setStatusTip("Copy selected results from the display to the clipborad");
	connect(copy_action, SIGNAL(triggered()), this, SLOT(CopyResults()));

	// Actions|Check
	check_action = new QAction("Check &System", this);
	check_action->setShortcut(tr("Ctrl+S"));
	check_action->setStatusTip("Run the currently loaded system through the checker");
	connect(check_action, SIGNAL(triggered()), this, SLOT(CheckSystem()));

	// Help|Help
	help_action = new QAction("&Help", this);
	help_action->setShortcut(tr("Ctrl+H"));
	help_action->setStatusTip("Go to Tesla online manual");
	connect(help_action, SIGNAL(triggered()), this, SLOT(HelpText()));

}

void	Tesla::CreateMainMenu()
{
	// File Menu
	file_menu = menuBar()->addMenu("&File");
	file_menu->addAction(load_action);
	file_menu->addSeparator();
	file_menu->addAction(exit_action);

	// Edit Menu
	edit_menu = menuBar()->addMenu("&Edit");
	edit_menu->addAction(clear_action);
	edit_menu->addAction(copy_action);
	edit_menu->addSeparator();
	edit_menu->addAction(font_action);

	// Actions Menu
	actions_menu = menuBar()->addMenu("&Actions");
	actions_menu->addAction(check_action);

	// Help Menu
	help_menu = menuBar()->addMenu("&Help");
	help_menu->addAction(help_action);
}

void	Tesla::CreateStatusBar()
{
	statusBar()->showMessage(tr("Ready"));
}

void	Tesla::DisplayFileName(const std::string& text)
{
	ui.FileListBox->addItem(QString::fromStdString(text));
}

bool Tesla::eventFilter(QObject *target, QEvent *event)
{
	if ((target == ui.ImageWidget) && (event->type() == QEvent::Paint))
	{
		QPainter	painter(ui.ImageWidget);
		painter.drawImage(0, 0, picture);
		return(true);
	}
	return(QWidget::eventFilter(target, event));
}

void	Tesla::Font()
{
	bool ok;
	QFont	original_font(ui.centralWidget->font());
	QFont font = QFontDialog::getFont(&ok, ui.centralWidget->font(), this);
	if (!ok)
		font = original_font;
	ui.centralWidget->setFont(font);
}

void	Tesla::HelpText()
{
	QString	url("http://www.ibgames.net/fed2/workbench/tesla/manual.html");
	QDesktopServices::openUrl(url);
}

void	Tesla::IssueError(const std::string& map_title, const std::string& message)
{
	std::ostringstream	buffer;
	buffer << "   Error - " << map_title << ": " << message;
	ui.ReportEdit->setTextColor(Qt::red);
	ui.ReportEdit->setFontWeight(QFont::Bold);
	ui.ReportEdit->append(QString::fromStdString(buffer.str()));
	ui.ReportEdit->setFontWeight(QFont::Normal);
	ui.ReportEdit->setTextColor(Qt::black);
	status = ARE_ERRORS;
}

void	Tesla::IssueStaffOnly(const std::string& map_title, const std::string& message)
{
	std::ostringstream	buffer;
	buffer << "   Staff Only - " << map_title << ": " << message;
	ui.ReportEdit->setTextColor(Qt::darkGreen);
	ui.ReportEdit->append(QString::fromStdString(buffer.str()));
	ui.ReportEdit->setTextColor(Qt::black);
}

void	Tesla::IssueWarning(const std::string& map_title, const std::string& message)
{
	std::ostringstream	buffer;
	buffer << "   Warning - " << map_title << ": " << message;
	ui.ReportEdit->setTextColor(Qt::blue);
	ui.ReportEdit->append(QString::fromStdString(buffer.str()));
	ui.ReportEdit->setTextColor(Qt::black);
}

void	Tesla::LoadFiles()
{
	if (directory == "")
		directory = "./";
	directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
		directory.c_str(), QFileDialog::ShowDirsOnly).QString::toStdString();
	NsTesla::loader->Load(directory);
}

void	Tesla::StatusMessage(const std::string& message)
{
	if (status == NO_ERRORS)
	{
		statusBar()->showMessage(tr(message.c_str()));
		ui.ReportEdit->append(QString::fromStdString(message));
	}
}

