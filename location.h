/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef LOCATION_H
#define LOCATION_H

#include	<bitset>
#include <list>
#include <string>

class FedMap;
class FedMessages;
class	LocFlags;

typedef	std::list<std::string>	LineList;

class Location
{
public:
	enum	// directions
	{
		NORTH, NE, EAST, SE, SOUTH, SW, WEST,
		NW, UP, DOWN, INTO, OUTOF, MAX_EXITS
	};
	enum	// flags
	{
		SPACE, LINK, EXCHANGE, SHIPYARD, REPAIR, HOSPITAL, INSURE, 
		PEACE, BAR, NO_DROP, TELEPORT, CUTH, AK, FIGHTING, WEAPONS,
		MAX_FLAGS
	};

	enum	{ IS_OK, NO_LOC, NO_LINK };	// link test
	enum	{ ENTER, BAD_EXIT, IN_ROOM, SEARCH, MAX_EVENTS };	// event triggers

	static const int	LINE_SIZE;
	static const int	NO_EXIT;
	static const int	INVALID_LOC;
	static const int	MAX_LOC_NO;
	static const int	NO_EVENT;

	int			loc_no;
	std::string	name;
	LineList		desc;
	int			exits[MAX_EXITS];
	std::string	no_exit;
	std::string	events[MAX_EVENTS];
	LocFlags		*flags;

public:
	Location();
	Location(int number);
	~Location();

	const std::string&	Name()								{ return(name); }

	int	HasLink(int dir, int to);
	int	Number()													{ return(loc_no); }

	bool	CheckNoExitFormat(FedMap *owner);
	bool	IsExchange()											{ return(TestFlag(EXCHANGE)); }
	bool	OutSystemFlagsOK(FedMap *owner);
	bool	PlanetFlagsOK(FedMap *owner);
	bool	SpaceFlagsOK(FedMap *owner);
	bool	TestFlag(int flag_no);
	bool	TestMovementTable(FedMap *owner);
	bool	TestNoExitMessage(FedMap *owner, FedMessages *messages);

	void	AddDesc(const std::string& text)					{ desc.push_back(text); }
	void	AddEvent(int which, const std::string& event)	{ events[which] = event; }
	void	AddExit(int dir, int dest)							{ exits[dir] = dest; }
	void	AddFlags(const std::string& flags_str);
	void	AddName(const std::string& the_name)			{ name = the_name; }
	void	AddNoExit(const std::string& msg_num)			{ no_exit = msg_num; }
	void	TestTextPresent(FedMap *owner);
};

#endif
