/*-----------------------------------------------------------------------
           Tesla - Windows Federation 2 Star System Checker
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and /or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef LOCFLAGS_H
#define LOCFLAGS_H

#include	<bitset>

class FedMap;

class	LocFlags
{
public:
	enum // Eeep! What's this - why do we have two duplicate sets of flags?
	{
		SPACE, LINK, EXCHANGE, SHIPYARD, REPAIR, HOSPITAL, INSURE, 
		PEACE, BAR, NO_DROP, TELEPORT, CUTH, AK, FIGHTING, WEAPONS,
		MAX_FLAGS
	};
	
private:
	std::bitset<MAX_FLAGS>	flags;

public:
	LocFlags()	{	}
	LocFlags(const std::string& flag_string);
	~LocFlags()	{	}

	bool	PlanetFlagsOK(FedMap *owner,int loc_no);
	bool	OutSystemFlagsOK(FedMap *owner,int loc_no);
	bool	SpaceFlagsOK(FedMap *owner,int loc_no);
	bool	Test(int flag_no)		{ return(flags.test(flag_no));	}
};

#endif
