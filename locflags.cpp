/*-----------------------------------------------------------------------
           Tesla - Windows Federation 2 Star System Checker
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "locflags.h"

#include <sstream>

#include "fedmap.h"
#include "tesla.h"

LocFlags::LocFlags(const std::string& flags_string)
{
	static const std::string	flag_names = "sleyrhipbktcafw";
	static const int	names_len = flag_names.length();
	
	int	str_len = flags_string.length();
	for(int count = 0;count < str_len;count++)
	{
		for(int index = 0;index < names_len;index++)
		{
			if(flags_string[count] == flag_names[index])
			{
				flags.set(index);
				break;
			}
		}
	}
}


bool	LocFlags::OutSystemFlagsOK(FedMap *owner,int loc_no)
{
	std::ostringstream	buffer;
	if(flags.test(CUTH))	
	{
		buffer << "Location # " <<loc_no << " - Courier property set on out system planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}
	
	if(flags.test(AK))	
	{
		buffer << "Location # " <<loc_no << " - Pickup property set on out system planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}
	
	return(true);
}

bool	LocFlags::PlanetFlagsOK(FedMap *owner,int loc_no)
{
	std::ostringstream	buffer;
	if(flags.test(SPACE))	
	{
		buffer << "Location # " <<loc_no << " - space property set on planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}
	
	if(flags.test(LINK))	
	{
		buffer << "Location # " <<loc_no << " - hyperspace link property set on planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}
	
	if(flags.test(PEACE))	
	{
		buffer << "Location # " <<loc_no << " - peace property set on planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(FIGHTING))	
	{
		buffer << "Location # " <<loc_no << " - fighting property set on planet!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(REPAIR) && flags.test(EXCHANGE))	
	{
		buffer << "Location # " <<loc_no << " - has both exchange and repair shop flags set!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	return(true);
}

bool	LocFlags::SpaceFlagsOK(FedMap *owner,int loc_no)
{
	std::ostringstream	buffer;
	if(!flags.test(SPACE))	
	{
		buffer << "Location # " << loc_no << " - no space property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(EXCHANGE))
	{
		buffer << "Location # " <<loc_no << " - exchange property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(SHIPYARD))
	{
		buffer << "Location # " <<loc_no << " - shipyard property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(REPAIR))
	{
		buffer << "Location # " <<loc_no << " - repair property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(HOSPITAL))
	{
		buffer << "Location # " <<loc_no << " - hospital property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(INSURE))
	{
		buffer << "Location # " <<loc_no << " - insure property set on space locationt!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(BAR))
	{
		buffer << "Location # " <<loc_no << " - bar property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(WEAPONS))
	{
		buffer << "Location # " <<loc_no << " - weapons shop property set on space location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	if(flags.test(LINK) && flags.test(FIGHTING))
	{
		buffer << "Location # " <<loc_no << " - fighting flag set on link location!";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	return(true);
}
	
