/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "event_handler.h"

#include "fedevents.h"
#include "misc.h"
#include "event.h"
#include "scr_announce.h"
#include	"scr_call.h"
#include "scr_changemoney.h"
#include "scr_changeplayer.h"
#include "scr_checkforowner.h"
#include "scr_checkgender.h"
#include "scr_checkinsurance.h"
#include "scr_checklastloc.h"
#include "scr_checkmoney.h"
#include "scr_checkplayer.h"
#include "scr_checkrank.h"
#include "scr_destroyinv.h"
#include "scr_match.h"
#include "scr_message.h"
#include "scr_move.h"
#include "scr_nomatch.h"
#include "scr_percent.h"
#include "star.h"
#include "tesla.h"


const std::string	EventHandler::el_names[] =
{
	"event-list", "category", "section", "comment", "event",				//  0- 4
	"announce", "call", "checkforowner", "checkgender", "checklastloc",	//	 5- 9
	"message", "move", "percent", "checkmoney", "changemoney",		//	10-14
	"checkplayer", "changeplayer", "checkrank", "checkinsurance", "match",				//	15-19
	"nomatch", "destroyinv",
	""
};

const int	EventHandler::NO_ELEMENT = 99999;

EventHandler::~EventHandler()
{
	// Make sure destructors are called for the whole heirarchy
}


bool	EventHandler::characters(const QString& ch)
{
	if (text_needed != NO_TEXT)
		buffer += ch.toStdString();
	return(true);
}

bool	EventHandler::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	std::ostringstream	buff;
	switch (FindElement(qName.toStdString()))
	{
	case  0:	NsTesla::star->AddEvents(fed_events);
		buff << fed_events->FileRoot() << ".xev";
		Tesla::Instance()->DisplayFileName(buff.str());
		break;
	case  4:	EndEvent();				break;
	case  5:											// drop through
	case 10: EndTextTypeScript();	break;
	}

	return(true);
}

void	EventHandler::EndEvent()
{
	if (current_event != 0)
	{
		if (fed_events != 0)
			fed_events->AddEvent(current_event);
		current_event = 0;
	}
}

void	EventHandler::EndTextTypeScript()
{
	if (current_script != 0)
		current_script->AddText(buffer);
	if (current_event != 0)
		current_event->AddScript(current_script);
	else
		delete current_script;
	current_script = 0;
	buffer = "";
	text_needed = NO_TEXT;
}

int	EventHandler::FindElement(const std::string& elem) const
{
	int which;
	for (which = 0; el_names[which] != ""; which++)
	{
		if (elem == el_names[which])
			return(which);
	}
	return(NO_ELEMENT);
}

void	EventHandler::StartAnnounce(const QXmlAttributes& attribs)
{
	int loc_no = attribs.value(QString::fromLatin1("loc")).toInt();
	std::string	low_mssg = attribs.value(QString::fromLatin1("lo")).toStdString();
	std::string	high_mssg = attribs.value(QString::fromLatin1("hi")).toStdString();
	current_script = new Announce(loc_no, low_mssg, high_mssg);
	buffer = "";
	text_needed = EVENT_ELEM;
}

void	EventHandler::StartCall(const QXmlAttributes& attribs)
{
	std::string	ev_str = attribs.value(QString::fromLatin1("event")).toStdString();
	Script	*script = new Call(ev_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCategory(const QXmlAttributes& attribs)
{
	current_cat = attribs.value(QString::fromLatin1("name")).toStdString();
}

void	EventHandler::StartChangeMoney(const QXmlAttributes& attribs)
{
	int		val = attribs.value(QString::fromLatin1("amount")).toInt();
	Script	*script = new ChangeMoney(val);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartChangePlayer(const QXmlAttributes& attribs)
{
	int	sta = attribs.value(QString::fromLatin1("sta")).toInt();
	int	str = attribs.value(QString::fromLatin1("str")).toInt();
	int	dex = attribs.value(QString::fromLatin1("dex")).toInt();
	int	intel = attribs.value(QString::fromLatin1("int")).toInt();

	Script	*script = new ChangePlayer(sta, str, dex, intel);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckForOwner(const QXmlAttributes& attribs)
{
	std::string	owner_str = attribs.value(QString::fromLatin1("owner")).toStdString();
	std::string	visitor_str = attribs.value(QString::fromLatin1("visitor")).toStdString();
	Script	*script = new CheckForOwner(owner_str, visitor_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckGender(const QXmlAttributes& attribs)
{
	std::string	male_str = attribs.value(QString::fromLatin1("male")).toStdString();
	std::string	female_str = attribs.value(QString::fromLatin1("female")).toStdString();
	std::string	neuter_str = attribs.value(QString::fromLatin1("neuter")).toStdString();
	Script	*script = new CheckGender(male_str, female_str, neuter_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckInsurance(const QXmlAttributes& attribs)
{
	std::string	pass_str = attribs.value(QString::fromLatin1("pass")).toStdString();
	std::string	fail_str = attribs.value(QString::fromLatin1("fail")).toStdString();
	Script	*script = new CheckInsurance(pass_str, fail_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckLastLoc(const QXmlAttributes& attribs)
{
	int			loc_num = attribs.value(QString::fromLatin1("loc")).toInt();
	std::string	pass_str = attribs.value(QString::fromLatin1("pass")).toStdString();
	std::string	fail_str = attribs.value(QString::fromLatin1("fail")).toStdString();
	Script	*script = new CheckLastLoc(loc_num, pass_str, fail_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckMoney(const QXmlAttributes& attribs)
{
	int			val = attribs.value(QString::fromLatin1("amount")).toInt();
	std::string	high_str = attribs.value(QString::fromLatin1("higher")).toStdString();
	std::string	eq_str = attribs.value(QString::fromLatin1("equals")).toStdString();
	std::string	low_str = attribs.value(QString::fromLatin1("lower")).toStdString();
	Script	*script = new CheckMoney(val, high_str, eq_str, low_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckPlayer(const QXmlAttributes& attribs)
{
	int	sta = attribs.value(QString::fromLatin1("sta")).toInt();
	int	str = attribs.value(QString::fromLatin1("str")).toInt();
	int	dex = attribs.value(QString::fromLatin1("dex")).toInt();
	int	intel = attribs.value(QString::fromLatin1("int")).toInt();

	std::string	pass_str = attribs.value(QString::fromLatin1("pass")).toStdString();
	std::string	fail_str = attribs.value(QString::fromLatin1("fail")).toStdString();

	Script	*script = new CheckPlayer(sta, str, dex, intel, pass_str, fail_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartCheckRank(const QXmlAttributes& attribs)
{
	std::string	level = attribs.value(QString::fromLatin1("level")).toStdString();
	std::string	high_str = attribs.value(QString::fromLatin1("higher")).toStdString();
	std::string	eq_str = attribs.value(QString::fromLatin1("equals")).toStdString();
	std::string	low_str = attribs.value(QString::fromLatin1("lower")).toStdString();
	Script	*script = new CheckRank(level, high_str, eq_str, low_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartDestroyInv(const QXmlAttributes& attribs)
{
	std::string	title = attribs.value(QString::fromLatin1("title")).toStdString();
	Script	*script = new DestroyInv(title);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

bool	EventHandler::startElement(const QString& namespaceURI, const QString& localName,
	const QString& qName, const QXmlAttributes& atts)
{
	switch (FindElement(qName.toStdString()))
	{
	case  0:										break;	// event-list
	case  1:	StartCategory(atts);			break;
	case  2:	StartSection(atts);			break;
	case  3:										break;	// comment
	case  4:	StartEvent(atts);				break;
	case  5: StartAnnounce(atts);			break;
	case  6:	StartCall(atts);				break;
	case  7:	StartCheckForOwner(atts);	break;
	case  8:	StartCheckGender(atts);		break;
	case  9:	StartCheckLastLoc(atts);	break;
	case 10:	StartMessage(atts);			break;
	case 11:	StartMove(atts);				break;
	case 12:	StartPercent(atts);			break;
	case 13:	StartCheckMoney(atts);		break;
	case 14:	StartChangeMoney(atts);		break;
	case 15:	StartCheckPlayer(atts);		break;
	case 16:	StartChangePlayer(atts);	break;
	case 17:	StartCheckRank(atts);		break;
	case 18:	StartCheckInsurance(atts);	break;
	case 19:	StartMatch(atts);				break;
	case 20:	StartNoMatch(atts);			break;
	case 21:	StartDestroyInv(atts);		break;
	}
	return(true);
}

void	EventHandler::StartEvent(const QXmlAttributes& attribs)
{
	std::ostringstream	buff;
	buff << current_cat << "." << current_sect << "." << attribs.value(QString::fromLatin1("num")).toInt();
	current_event = new Event(buff.str());
	text_needed = NO_TEXT;
	buffer = "";
}

void	EventHandler::StartMatch(const QXmlAttributes& attribs)
{
	std::string	id = attribs.value(QString::fromLatin1("id-name")).toStdString();
	std::string	phrase = attribs.value(QString::fromLatin1("phrase")).toStdString();
	std::string	low = attribs.value(QString::fromLatin1("lo")).toStdString();
	std::string	high = attribs.value(QString::fromLatin1("hi")).toStdString();
	std::string	event = attribs.value(QString::fromLatin1("event")).toStdString();
	Script	*script = new Match(id, phrase, low, high, event);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartMessage(const QXmlAttributes& attribs)
{
	std::string	low_mssg = attribs.value(QString::fromLatin1("lo")).toStdString();
	std::string	high_mssg = attribs.value(QString::fromLatin1("hi")).toStdString();
	current_script = new MessageScript(low_mssg, high_mssg);
	buffer = "";
	text_needed = EVENT_ELEM;
}

void	EventHandler::StartMove(const QXmlAttributes& attribs)
{
	int	loc_no = attribs.value(QString::fromLatin1("loc")).toInt();
	Script	*script = new Move(loc_no);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartNoMatch(const QXmlAttributes& attribs)
{
	std::string	id = attribs.value(QString::fromLatin1("id-name")).toStdString();
	std::string	low = attribs.value(QString::fromLatin1("lo")).toStdString();
	std::string	high = attribs.value(QString::fromLatin1("hi")).toStdString();
	std::string	event = attribs.value(QString::fromLatin1("event")).toStdString();
	Script	*script = new NoMatch(id, low, high, event);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartPercent(const QXmlAttributes& attribs)
{
	int			val = attribs.value(QString::fromLatin1("value")).toInt();
	std::string	high_str = attribs.value(QString::fromLatin1("higher")).toStdString();
	std::string	eq_str = attribs.value(QString::fromLatin1("equals")).toStdString();
	std::string	low_str = attribs.value(QString::fromLatin1("lower")).toStdString();
	Script	*script = new Percent(val, high_str, eq_str, low_str);
	if (current_event != 0)
		current_event->AddScript(script);
	else
		delete script;
}

void	EventHandler::StartSection(const QXmlAttributes& attribs)
{
	current_sect = attribs.value(QString::fromLatin1("name")).toStdString();
}

