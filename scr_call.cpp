/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "scr_call.h"

#include <sstream>

#include "event.h"
#include "fedevents.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"

bool	Call::Check(const std::string file_name, const std::string event_id,
	FedMap *fed_map, FedMessages *messages, FedEvents *fed_events)
{
	return(EventExists(file_name, event_id, event_str, " - Checkgender - event ", fed_events));
}

