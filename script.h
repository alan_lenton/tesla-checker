/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SCRIPT_H
#define SCRIPT_H

#include <string>

class	FedEvents;
class	FedMap;
class	FedMessages;

class	Script
{
protected:
	static bool	EventExists(const std::string file_name, const std::string& event_id,
		const std::string& event_str, const std::string& text, FedEvents *fed_events);
	static bool	MessageExists(const std::string file_name, const std::string& event_id,
		const std::string& mssg_str, const std::string& text, FedMessages *messages);

public:
	Script()		{	}
	virtual ~Script()	{	}

	virtual bool	Check(const std::string file_name, const std::string event_id,
		FedMap *fed_map, FedMessages *messages, FedEvents *fed_events) = 0;
	virtual void	AddText(const std::string& new_text)		{	}
};

#endif

