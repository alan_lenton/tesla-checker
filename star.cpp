/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "star.h"

#include "fedevents.h"
#include "fedmap.h"
#include "fedmssgs.h"
#include "misc.h"
#include "tesla.h"

void	Star::AddEvents(FedEvents *fed_events)
{
	event_index[fed_events->FileRoot()] = fed_events;
}

void	Star::AddMap(FedMap *fed_map)
{
	map_index[fed_map->FileRoot()] = fed_map;
}

void	Star::AddMessages(FedMessages *fed_mssg)
{
	mssg_index[fed_mssg->FileRoot()] = fed_mssg;
}

void	Star::Clear()
{
	for (MapIndex::iterator iter = map_index.begin(); iter != map_index.end(); ++iter)
		delete iter->second;
	map_index.clear();
	Tesla::Instance()->ClearFileList();
	for (MssgIndex::iterator iter = mssg_index.begin(); iter != mssg_index.end(); ++iter)
		delete iter->second;
	mssg_index.clear();

	for (FedEventIndex::iterator iter = event_index.begin(); iter != event_index.end(); ++iter)
		delete iter->second;
	event_index.clear();
}

FedEvents	*Star::FindEvents(const std::string& root)
{
	FedEventIndex::iterator	iter = event_index.find(root);
	if (iter != event_index.end())
		return(iter->second);
	else
		return(0);
}

FedMap	*Star::FindMap(const std::string& root)
{
	MapIndex::iterator	iter = map_index.find(root);
	if (iter != map_index.end())
		return(iter->second);
	else
		return(0);
}

FedMessages	*Star::FindMssg(const std::string& root)
{
	MssgIndex::iterator iter = mssg_index.find(root);
	if (iter != mssg_index.end())
		return(iter->second);
	else
		return(0);
}


bool	Star::OrbitExists(const std::string& root, const std::string& orbit)
{
	std::ostringstream	buffer;
	buffer << "Orbit information - " << orbit << " - is incorrect!";

	std::string::size_type	start = 0;
	std::string::size_type	end = orbit.find('.');
	if (end == std::string::npos)
	{
		Tesla::Instance()->IssueError(root, buffer.str());
		return(false);
	}

	std::string	system_name = orbit.substr(start, end - start);
	start = end + 1;
	end = orbit.find('.', start);
	if (end == std::string::npos)
	{
		Tesla::Instance()->IssueError(root, buffer.str());
		return(false);
	}

	std::string	map_title = orbit.substr(start, end - start);
	start = end + 1;
	if (start >= orbit.length())
	{
		Tesla::Instance()->IssueError(root, buffer.str());
		return(false);
	}

	int	orbit_num = std::atoi(orbit.substr(start).c_str());

	if (isupper(system_name[0]) == 0)
	{
		Tesla::Instance()->IssueError(root,
			"Orbit location system name must start with an upper case letter!");
		return(false);
	}

	if (map_title.find(system_name) != 0)
	{
		Tesla::Instance()->IssueError(root,
			"The space map title must be in the form 'System_name Space'!");
		return(false);
	}

	if (map_title.find("Space") == std::string::npos)
	{
		Tesla::Instance()->IssueError(root,
			"The space map title must be in the form 'System_name Space'!");
		return(false);
	}

	// now lets see if we can actually find the location!
	FedMap	*fed_map = FindMap("space");
	if (fed_map == 0)
	{
		Tesla::Instance()->IssueError(root, "The orbit map cannot be found!");
		return(false);
	}

	if (!fed_map->LocExists(orbit_num))
	{
		Tesla::Instance()->IssueError(root, "The orbit location cannot be found!");
		return(false);
	}

	return(true);
}

bool	Star::SpaceMapContains(const std::string& planet_name)
{
	for (MapIndex::iterator iter = map_index.begin(); iter != map_index.end(); ++iter)
	{
		if (iter->second->FileName() == "space.loc")
			return(iter->second->TitleContains(planet_name));
	}
	Tesla::Instance()->IssueError("", "No 'space.loc' file found!");
	return(true);
}

bool	Star::TestEvents()
{
	for (FedEventIndex::iterator iter = event_index.begin(); iter != event_index.end(); ++iter)
	{
		if (!iter->second->Test(FindMap(iter->second->FileRoot()), FindMssg(iter->second->FileRoot())))
			return(false);
	}
	return(true);
}

bool	Star::TestMap()
{
	for (MapIndex::iterator iter = map_index.begin(); iter != map_index.end(); ++iter)
	{
		if (!iter->second->TestLocations())
			return(false);
	}
	return(true);
}

bool	Star::TestMessages()
{
	for (MssgIndex::iterator iter = mssg_index.begin(); iter != mssg_index.end(); ++iter)
	{
		if (!iter->second->Test())
			return(false);
	}
	return(true);
}


