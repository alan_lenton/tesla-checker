/*-----------------------------------------------------------------------
           Tesla - Windows Federation 2 Star System Checker
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "scr_match.h"

#include <sstream>

#include "tesla.h"

bool	Match::Check(const std::string file_name,const std::string event_id,
											FedMap *fed_map, FedMessages *messages,FedEvents *fed_events)
{
	std::ostringstream	buffer;
	if((id_name == "") || (phrase == ""))
	{
		buffer  << event_id << " - Match - ID/name or phrase missing.";
		Tesla::Instance()->IssueError(file_name,buffer.str());
		return(false);
	}
	if((low == "") && (high == "") && (event == ""))
	{
		buffer  << event_id << " - Match - no actions provided.";
		Tesla::Instance()->IssueError(file_name,buffer.str());
		return(false);
	}

	if(!EventExists(file_name,event_id,event," - Match - event ",fed_events))	
		return(false);
	if(!CheckMessages(file_name,event_id,messages))	
		return(false);
	return(true);
}

bool	Match::CheckMessages(const std::string file_name,const std::string event_id,FedMessages *messages)
{
	if(!MessageExists(file_name,event_id,low," - Match - message number ",messages))
		return(false);
	if(!MessageExists(file_name,event_id,high," - Match - message number ",messages))
		return(false);

	return(true);
}

