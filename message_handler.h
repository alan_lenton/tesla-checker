/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <QString>
#include <QXmlAttributes>
#include <QXmlDefaultHandler>
#include <QXmlParseException>
#include <QXmlSimpleReader>

#include <string>

class	FedMessages;
class	Message;

class	MessageHandler : public QXmlDefaultHandler
{
private:
	static const std::string	el_names[];
	static const int	NO_ELEMENT;

	enum	{ NO_TEXT, MESSAGE_ELEM };

	FedMessages	*fed_mssgs;
	std::string	current_cat;
	std::string	current_sect;
	Message		*current_message;

	int			text_needed;
	std::string	buffer;

	int	FindElement(const std::string& elem) const;

	void	EndMessage();
	void	StartCategory(const QXmlAttributes& attribs);
	void	StartMessage(const QXmlAttributes& attribs);
	void	StartSection(const QXmlAttributes& attribs);

public:
	MessageHandler(FedMessages	*f_mssgs) : fed_mssgs(f_mssgs), current_cat(""),
		current_sect(""), current_message(0), text_needed(NO_TEXT)	{	}
	virtual	~MessageHandler();

	bool	endElement(const QString& namespaceURI, const QString& localName, const QString& qName);
	bool	startElement(const QString& namespaceURI, const QString& localName,
		const QString& qName, const QXmlAttributes& atts);
	bool	characters(const QString& ch);
};

#endif
