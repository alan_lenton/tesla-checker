/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SCRCHECKINSURANCE_H
#define SCRCHECKINSURANCE_H

#include "script.h"

#include <string>

class CheckInsurance : public Script
{
private:
	std::string	pass;
	std::string	fail;


public:
	CheckInsurance(const std::string& pass_str, const std::string& fail_str) :
		pass(pass_str), fail(fail_str)	{	}
	virtual ~CheckInsurance()	{	}

	bool	Check(const std::string file_name, const std::string event_id,
		FedMap *fed_map, FedMessages *messages, FedEvents *fed_events);
};

#endif

