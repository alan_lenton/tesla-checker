/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "scr_announce.h"

#include <sstream>

#include "location.h"
#include "fedevents.h"
#include "fedmap.h"
#include "fedmssgs.h"
#include "message.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"


bool	Announce::Check(const std::string file_name, const std::string event_id,
	FedMap *fed_map, FedMessages *messages, FedEvents *fed_events)
{
	if (!CheckLocNum(file_name, event_id, fed_map))		return(false);
	if (!CheckMessages(file_name, event_id, messages))	return(false);
	if (!CheckText(file_name, event_id))					return(false);
	return(true);
}

bool	Announce::CheckLocNum(const std::string file_name, const std::string event_id, FedMap *fed_map)
{
	std::ostringstream	buffer;

	if ((loc_num < 0) || loc_num >(Location::MAX_LOC_NO))
	{
		buffer << event_id << " - Announce - location number is invalid.";
		Tesla::Instance()->IssueError(file_name, buffer.str());
		return(false);
	}

	if (!fed_map->LocExists(loc_num))
	{
		buffer << event_id << " - Announce - location number does not exist.";
		Tesla::Instance()->IssueError(file_name, buffer.str());
		return(false);
	}

	return(true);
}

bool	Announce::CheckMessages(const std::string file_name, const std::string event_id, FedMessages *messages)
{
	std::ostringstream	buffer;

	if ((low == "") && (high == "") && (text == ""))
	{
		buffer << event_id << " - Announce - no message provided.";
		Tesla::Instance()->IssueError(file_name, buffer.str());
		return(false);
	}

	if (!MessageExists(file_name, event_id, low, " - Announce - message number ", messages))
		return(false);
	if (!MessageExists(file_name, event_id, high, " - Announce - message number ", messages))
		return(false);

	return(CheckText(file_name, event_id));
}

bool	Announce::CheckText(const std::string file_name, const std::string event_id)
{
	// need to figure out what we need to test here!
	return(true);
}

