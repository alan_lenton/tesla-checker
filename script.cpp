/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "script.h"

#include <sstream>

#include "event.h"
#include "fedevents.h"
#include "fedmssgs.h"
#include "message.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"

bool	Script::EventExists(const std::string file_name, const std::string& event_id,
	const std::string& event_str, const std::string& text, FedEvents *fed_events)
{
	if (event_str != "")
	{
		//don't test random events yet
		if (event_str.find("random") != std::string::npos)
			return(true);

		Event	*event = fed_events->FindEvent(event_str);
		if (event == 0)
		{
			std::ostringstream	buffer;
			buffer << event_id << text << event_str << " doesn't exist.";
			Tesla::Instance()->IssueError(file_name, buffer.str());
			return(false);
		}
	}
	return(true);
}

bool	Script::MessageExists(const std::string file_name, const std::string& event_id,
	const std::string& mssg_str, const std::string& text, FedMessages *messages)
{
	if (mssg_str != "")
	{
		Message *message = messages->FindMessage(mssg_str);
		if (message == 0)
		{
			std::ostringstream	buffer;
			buffer << event_id << text << mssg_str << " doesn't exist.";
			Tesla::Instance()->IssueError(file_name, buffer.str());
			return(false);
		}
	}
	return(true);
}

