/*-----------------------------------------------------------------------
           Tesla - Windows Federation 2 Star System Checker
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef SCRNOMATCH_H
#define SCRNoMATCH_H

#include "script.h"

#include <string>

class NoMatch : public Script
{
private:
	std::string	id_name;
	std::string	low;
	std::string	high;
	std::string	event;

	bool	CheckMessages(const std::string file_name,const std::string event_id,FedMessages *messages);

public:
	NoMatch(const std::string& name,const std::string& low_mssg,
								const std::string& high_mssg,const std::string& evt) : 
								id_name(name), low(low_mssg), high(high_mssg), event(evt)	{	}
	virtual ~NoMatch()	{	}

	bool	Check(const std::string file_name,const std::string event_id,FedMap *fed_map, 
																		FedMessages *messages,FedEvents *fed_events);
};

#endif

