/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "message_handler.h"

#include "fedmssgs.h"
#include "misc.h"
#include "message.h"
#include "star.h"
#include "tesla.h"


const std::string	MessageHandler::el_names[] =
{ "para-list", "category", "section", "para", "" };

const int	MessageHandler::NO_ELEMENT = 99999;

MessageHandler::~MessageHandler()
{
	// Make sure destructors are called for the whole heirarchy
}


bool	MessageHandler::characters(const QString& ch)
{
	if (text_needed != NO_TEXT)
		buffer += ch.toStdString();
	return(true);
}

bool	MessageHandler::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	std::ostringstream	buff;
	switch (FindElement(qName.toStdString()))
	{
	case 0:	NsTesla::star->AddMessages(fed_mssgs);
		buff << fed_mssgs->FileRoot() << ".msg";
		Tesla::Instance()->DisplayFileName(buff.str());
		break;
	case 3:	EndMessage();			break;
	}
	return(true);
}

void	MessageHandler::EndMessage()
{
	if (current_message != 0)
	{
		current_message->Text(buffer);
		text_needed = NO_TEXT;
		if (fed_mssgs != 0)
			fed_mssgs->AddMessage(current_message);
		current_message = 0;
	}
}

int	MessageHandler::FindElement(const std::string& elem) const
{
	int which;
	for (which = 0; el_names[which] != ""; which++)
	{
		if (elem == el_names[which])
			return(which);
	}
	return(NO_ELEMENT);
}

void	MessageHandler::StartCategory(const QXmlAttributes& attribs)
{
	current_cat = attribs.value(QString::fromLatin1("name")).toStdString();
}

bool	MessageHandler::startElement(const QString& namespaceURI, const QString& localName,
	const QString& qName, const QXmlAttributes& atts)
{
	switch (FindElement(qName.toStdString()))
	{
	case 0:									break;
	case 1:	StartCategory(atts);		break;
	case 2:	StartSection(atts);		break;
	case 3:	StartMessage(atts);		break;
	}
	return(true);
}

void	MessageHandler::StartMessage(const QXmlAttributes& attribs)
{
	std::ostringstream	buff;
	buff << current_cat << "." << current_sect << "." << attribs.value(QString::fromLatin1("number")).toInt();
	current_message = new Message(buff.str());
	text_needed = MESSAGE_ELEM;
	buffer = "";
}

void	MessageHandler::StartSection(const QXmlAttributes& attribs)
{
	current_sect = attribs.value(QString::fromLatin1("name")).toStdString();
}

