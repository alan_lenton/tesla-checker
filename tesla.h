/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef TESLA_H
#define TESLA_H

#include <QMainWindow>

#include <QAction>
#include <QLabel>
#include <QMenu>

#include <sstream>
#include <string>

#include "ui_tesla.h"

class Tesla : public QMainWindow
{
	Q_OBJECT

public:
	enum	{ NO_ERRORS, ARE_ERRORS };

private:
	static Tesla	*instance;

	Ui::TeslaClass ui;

	int	status;

	std::string	directory;
	std::string	version;

	QAction	*check_action;
	QAction	*clear_action;
	QAction	*copy_action;
	QAction	*exit_action;
	QAction	*font_action;
	QAction	*help_action;
	QAction	*load_action;

	QClipboard *clipboard;
	QImage	picture;

	QMenu		*actions_menu;
	QMenu		*edit_menu;
	QMenu		*file_menu;
	QMenu		*help_menu;

	bool	eventFilter(QObject *target, QEvent *event);

	void	CreateActions();
	void	CreateMainMenu();
	void	CreateStatusBar();

	private slots:
	void	CheckSystem();
	void	ClearReport();
	void	CopyResults();
	void	Font();
	void	HelpText();
	void	LoadFiles();

public:
	Tesla(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~Tesla();

	static Tesla *Instance()								{ return(instance); }

	const std::string&	GetDirectory()					{ return(directory); }

	bool	ContinueTests()									{ return(status == NO_ERRORS); }

	void	ClearFileList();
	void	Display(const char	*text)					{ ui.ReportEdit->append(tr(text)); }
	void	Display(const std::string& text)				{ ui.ReportEdit->append(tr(text.c_str())); }
	void	Display(const std::ostringstream& text)	{ ui.ReportEdit->append(tr(text.str().c_str())); }
	void	DisplayFileName(const std::string& text);
	void	IssueError(const std::string& map_title, const std::string& message);
	void	IssueStaffOnly(const std::string& map_title, const std::string& message);
	void	IssueWarning(const std::string& map_title, const std::string& message);
	void	StatusMessage(const std::string& message);

};

#endif // TESLA_H
