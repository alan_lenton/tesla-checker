/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MAPHANDLER_H
#define MAPHANDLER_H

#include <QString>
#include <QXmlAttributes>
#include <QXmlDefaultHandler>
#include <QXmlParseException>
#include <QXmlSimpleReader>

class	FedMap;
class	Location;

class	MapHandler : public QXmlDefaultHandler
{
private:
	static const std::string	el_names[];
	static const std::string	dir_names[];
	static const std::string	ev_names[];
	static const int	NO_ELEMENT;

	enum	{ NO_TEXT, NAME_ELEM, DESC_ELEM };

	FedMap		*fed_map;
	Location		*current;
	int			text_needed;
	std::string	buffer;

	int	FindElement(const std::string& elem);

	void	StartEvents(const QXmlAttributes& attribs);
	void	StartExits(const QXmlAttributes& attribs);
	void	StartLocation(const QXmlAttributes& attribs);
	void	StartMap(const QXmlAttributes& attribs);

public:
	MapHandler(FedMap	*f_map) : fed_map(f_map), current(0), text_needed(NO_TEXT)	{	}
	virtual	~MapHandler();

	bool	endElement(const QString& namespaceURI, const QString& localName, const QString& qName);
	bool	startElement(const QString& namespaceURI, const QString& localName,
		const QString& qName, const QXmlAttributes& atts);
	bool	characters(const QString& ch);

	//	bool	fatalError(const QXmlParseException& exception);
};

#endif

