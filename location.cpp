/*-----------------------------------------------------------------------
           Tesla - Windows Federation 2 Star System Checker
                  Copyright (c) 1985-2015 Alan Lenton

	This program is free software: you can redistribute it and/or modify 
	it under the terms of the GNU General Public License as published by 
	the Free Software Foundation: either version 3 of the License, or (at 
	your option) any later version.

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY: without even the implied warranty of 
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
	General Public License for more details.

	You can find the full text of the GNU General Public Licence at
               http://www.gnu.org/copyleft/gpl.html

	Programming and design: 	Alan Lenton (email: alan@ibgames.com)
	Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "location.h"

#include <sstream>

#include <cctype>

#include	"fedmap.h"
#include	"fedmssgs.h"
#include "locflags.h"
#include "tesla.h"

const int	Location::LINE_SIZE = 1024 + 80;
const int	Location::NO_EXIT = -1;
const int	Location::INVALID_LOC = -1;
const int	Location::MAX_LOC_NO = 4095;
const int	Location::NO_EVENT = -1;

Location::Location()
{
	loc_no = INVALID_LOC;
	for(int count = 0;count < MAX_EXITS;count++)
		exits[count] = NO_EXIT;
	for(int count = 0;count < MAX_EVENTS;count++)
		exits[count] = NO_EVENT;
	flags = 0;
}

Location::Location(int number)
{
	loc_no = number;
	for(int count = 0;count < MAX_EXITS;count++)
		exits[count] = NO_EXIT;
	for(int count = 0;count < MAX_EVENTS;count++)
		exits[count] = NO_EVENT;
	flags = 0;
}

Location::~Location()
{ 
	delete flags;	
}

void	Location::AddFlags(const std::string& flags_str)
{ 
	flags = new LocFlags(flags_str);	
}

int	Location::HasLink(int dir,int to)
{
	if(exits[dir] == to)
		return(IS_OK);
	else
		return(NO_LINK);
}

bool	Location::OutSystemFlagsOK(FedMap *owner)
{
	if(flags != 0)
		return(flags->OutSystemFlagsOK(owner,loc_no));
	return(true);
}

bool	Location::PlanetFlagsOK(FedMap *owner)
{
	if(flags != 0)
		return(flags->PlanetFlagsOK(owner,loc_no));
	return(true);
}

bool	Location::SpaceFlagsOK(FedMap *owner)
{
	if(flags != 0)
		return(flags->SpaceFlagsOK(owner,loc_no));

	std::ostringstream	buffer;
	buffer << "Location # " << loc_no << " - no space property set on space location!";
	Tesla::Instance()->IssueError(owner->Title(),buffer.str());
	return(false);
}

bool	Location::TestFlag(int flag_no)
{ 
	if(flags == 0)
		return(false);
	else
		return(flags->Test(flag_no));	
}

bool	Location::TestMovementTable(FedMap *owner)
{
	static const int	reciprocals[] = 
		{ SOUTH, SW, WEST, NW, NORTH, NE, EAST, SE, DOWN, UP, OUTOF, INTO	};
	static const std::string	dir_names[] =
	{
		"north", "northeast", "east", "southeast", "south", "southwest",
		"west", "northwest", "up", "down", "in", "out", ""
	};
	
	int	status = IS_OK;
	std::ostringstream	buffer;
	for(int count = 0;count < MAX_EXITS;count++)
	{
		if(exits[count] != NO_EXIT)
		{
			switch(owner->HasLink(exits[count],reciprocals[count],loc_no))
			{
				case	NO_LINK:
					if(count < INTO)	// in/out are not necessarily reciprocal
					{
						buffer.str("");
						buffer << "location " << loc_no << " " << dir_names[count];
						buffer << " link is one way only";
						Tesla::Instance()->IssueWarning(owner->Title(),buffer.str());
						buffer.str("");
					}
					break;

				case	NO_LOC:	status = NO_LOC;	break;
				case	IS_OK:							break;
			}
		}
	}
	
	return(status == IS_OK);
}

void	Location::TestTextPresent(FedMap *owner)
{
	std::ostringstream	buffer;
	
	if(name == "")
	{
		buffer.str("");
		buffer << "Location #" << loc_no << " has no name";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return;
	}
	if(name == "xxx")
	{
		buffer.str("");
		buffer << "Location #" << loc_no << " has no name";
		Tesla::Instance()->IssueWarning(owner->Title(),buffer.str());
	}
	
	if((desc.size() == 0) || ((desc.size() == 1) && (*(desc.begin()) == "")))
	{
		buffer.str("");
		buffer << "Location #" << loc_no << " has no description";
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return;
	}
	if(*(desc.begin()) == "xxx")
	{
		buffer.str("");
		buffer << "Location #" << loc_no << " has no description";
		Tesla::Instance()->IssueWarning(owner->Title(),buffer.str());
	}
}

bool	Location::CheckNoExitFormat(FedMap *owner)
{
	std::ostringstream	buffer	;
	buffer << "Location #" << loc_no << " no-exit message has the wrong format - see manual for correct format";

	if(no_exit == "")
		return(true);

	unsigned	len = no_exit.length();
	int dots = 0;
	for(unsigned count = 0; count < len;++count)
	{
		if(no_exit[count] =='.')
		{
			if(++dots > 2)
			{
				Tesla::Instance()->IssueError(owner->Title(),buffer.str());
				return(false);
			}
			else
				continue;
		}
			
		if(dots == 2)
		{
			if(std::isdigit(no_exit[count]) == 0)
			{
				Tesla::Instance()->IssueError(owner->Title(),buffer.str());
				return(false);
			}
		}
		else
		{
			if((std::isalpha(no_exit[count]) == 0) || (std::islower(no_exit[count]) == 0))
			{
				Tesla::Instance()->IssueError(owner->Title(),buffer.str());
				return(false);
			}
		}
	}
	
	if(dots < 2)
	{
		Tesla::Instance()->IssueError(owner->Title(),buffer.str());
		return(false);
	}

	return(true);
}

bool	Location::TestNoExitMessage(FedMap *owner,FedMessages *messages)
{
	std::ostringstream	buffer;
	if(no_exit != "")
	{
		if(messages == 0)
		{
			buffer << "Location #" << loc_no << " has a No Exit message, but there is no message file!";
			Tesla::Instance()->IssueError(owner->Title(),buffer.str());
			return(false);
		}
		else
		{
			if(messages->FindMessage(no_exit) == 0)
			{
				buffer << "Location #" << loc_no << " has a No Exit message which is not in the message file!";
				Tesla::Instance()->IssueError(owner->Title(),buffer.str());
				return(false);
			}
		}
	}
	
	return(true);
}
