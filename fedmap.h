/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef FEDMAP_H
#define FEDMAP_H

#include <map>
#include <string>

class	Location;

typedef	std::map<int, Location *, std::less<int> >	LocIndex;

class FedMap
{
	friend class	MapHandler;

public:
	static const int	HEIGHT = 64;
	static const int	WIDTH = 64;

private:
	std::string	file_name;
	std::string	file_root;
	std::string	title;
	std::string	launch_loc;
	std::string	orbit_loc;
	LocIndex	loc_index;

	bool	passed;

	bool	TestExchanges();
	bool	TestHyperSpaceLink();
	bool	TestLaunchOrbit();
	bool	TestNoExitMessages();
	bool	TestTitle();
	bool	TestTitleNormalised();

public:
	FedMap(const std::string& f_name);
	~FedMap();

	const std::string&	FileName()	{ return(file_name); }
	const std::string&	FileRoot()	{ return(file_root); }
	const std::string&	Title()		{ return(title); }

	int	HasLink(int from, int dir, int to);

	bool	LocExists(int number);
	bool	TestLocations();
	bool	TitleContains(const std::string& name);

	void	AddLocation(Location *loc);
	void	CheckEdges(int loc_no = -1);		// loc_no == -1 to reinitialise
};

#endif
