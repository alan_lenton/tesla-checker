/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include <QString>
#include <QXmlAttributes>
#include <QXmlDefaultHandler>
#include <QXmlParseException>
#include <QXmlSimpleReader>

#include <string>

class	FedEvents;
class	Event;
class	Script;

class	EventHandler : public QXmlDefaultHandler
{
private:
	static const std::string	el_names[];
	static const int	NO_ELEMENT;

	enum	{ NO_TEXT, EVENT_ELEM };

	FedEvents	*fed_events;
	std::string	current_cat;
	std::string	current_sect;
	Event			*current_event;
	Script		*current_script;

	int			text_needed;
	std::string	buffer;

	int	FindElement(const std::string& elem) const;

	void	EndTextTypeScript();
	void	EndEvent();
	void	StartAnnounce(const QXmlAttributes& attribs);
	void	StartCall(const QXmlAttributes& attribs);
	void	StartCategory(const QXmlAttributes& attribs);
	void	StartChangeMoney(const QXmlAttributes& attribs);
	void	StartChangePlayer(const QXmlAttributes& attribs);
	void	StartCheckForOwner(const QXmlAttributes& attribs);
	void	StartCheckGender(const QXmlAttributes& attribs);
	void	StartCheckInsurance(const QXmlAttributes& attribs);
	void	StartCheckLastLoc(const QXmlAttributes& attribs);
	void	StartCheckMoney(const QXmlAttributes& attribs);
	void	StartCheckPlayer(const QXmlAttributes& attribs);
	void	StartCheckRank(const QXmlAttributes& attribs);
	void	StartDestroyInv(const QXmlAttributes& attribs);
	void	StartEvent(const QXmlAttributes& attribs);
	void	StartMatch(const QXmlAttributes& attribs);
	void	StartMessage(const QXmlAttributes& attribs);
	void	StartMove(const QXmlAttributes& attribs);
	void	StartNoMatch(const QXmlAttributes& attribs);
	void	StartPercent(const QXmlAttributes& attribs);
	void	StartScript(const QXmlAttributes& attribs);
	void	StartSection(const QXmlAttributes& attribs);

public:
	EventHandler(FedEvents	*f_events) : fed_events(f_events), current_cat(""),
		current_sect(""), current_event(0), current_script(0), text_needed(NO_TEXT)	{	}
	virtual	~EventHandler();

	bool	endElement(const QString& namespaceURI, const QString& localName, const QString& qName);
	bool	startElement(const QString& namespaceURI, const QString& localName,
		const QString& qName, const QXmlAttributes& atts);
	bool	characters(const QString& ch);
};

#endif
