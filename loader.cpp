/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2013 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "loader.h"

#include <QDir>
#include <QFile>
#include <QXmlSimpleReader>

#include <sstream>
#include <cctype>

#include "fedevents.h"
#include "fedmap.h"
#include "fedmssgs.h"
#include	"event_handler.h"
#include "maphandler.h"
#include "message_handler.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"

void	Loader::CheckFileName(const std::string& file_name)
{
	int	len = file_name.length();
	for (int count = 0; count < len; count++)
	{
		if (std::isupper(file_name[count]) != 0)
			Tesla::Instance()->IssueError(file_name, "Files must have lower case file names!");
		if ((std::isalnum(file_name[count]) == 0) && (file_name[count] != '.'))
			Tesla::Instance()->IssueError(file_name, "Files must only use letters and numbers in their file names!");
	}
}

void	Loader::Load(const std::string& dir_name)
{
	static std::string	filter_elements[] = { "*.loc", "*.msg", "*.xev", "" };

	NsTesla::star->Clear();

	Tesla::Instance()->Display("  ");
	std::ostringstream	buffer;
	buffer << "Directory is " << dir_name;
	Tesla::Instance()->Display(buffer);
	Tesla::Instance()->Display("  ");

	QDir	dir(dir_name.c_str());
	QDir::setCurrent(dir_name.c_str());

	QStringList	filters;
	for (int count = 0; filter_elements[count] != ""; ++count)
	{
		filters.clear();
		filters += filter_elements[count].c_str();
		QStringList	file_list = dir.entryList(filters);
		QStringList::iterator	iter;
		for (iter = file_list.begin(); iter != file_list.end(); iter++)
		{
			Tesla::Instance()->StatusMessage("Loading " + (*iter).toStdString());
			switch (count)
			{
			case 0:	LoadLocFile(*iter);		break;
			case 1:	LoadMssgFile(*iter);		break;
			case 2:	LoadEventFile(*iter);	break;
			default:	break;
			}
		}
	}
}

void	Loader::LoadEventFile(const QString& file_name)
{
	CheckFileName(file_name.toStdString());
	QFile	file(file_name);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		FedEvents			*fed_events = new FedEvents(file_name.toStdString());
		QXmlInputSource	source(&file);
		EventHandler		handler(fed_events);
		QXmlSimpleReader	reader;

		reader.setContentHandler(&handler);
		reader.parse(source);
		file.close();
	}
	else
		Tesla::Instance()->IssueError(file_name.toStdString(), " - cannot read file");
}

void	Loader::LoadLocFile(const QString& file_name)
{
	CheckFileName(file_name.toStdString());
	QFile	file(file_name);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		FedMap				*fed_map = new FedMap(file_name.toStdString());
		QXmlInputSource	source(&file);
		MapHandler			handler(fed_map);
		QXmlSimpleReader	reader;

		reader.setContentHandler(&handler);
		reader.parse(source);
		file.close();
	}
	else
		Tesla::Instance()->IssueError(file_name.toStdString(), " - cannot read file");
}

void	Loader::LoadMssgFile(const QString& file_name)
{
	CheckFileName(file_name.toStdString());
	QFile	file(file_name);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		FedMessages			*fed_messages = new FedMessages(file_name.toStdString());
		QXmlInputSource	source(&file);
		MessageHandler		handler(fed_messages);
		QXmlSimpleReader	reader;

		reader.setContentHandler(&handler);
		reader.parse(source);
		file.close();
	}
	else
		Tesla::Instance()->IssueError(file_name.toStdString(), " - cannot read file");
}


