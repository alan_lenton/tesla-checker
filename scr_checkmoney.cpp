/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "scr_checkmoney.h"

#include <sstream>

#include "event.h"
#include "fedevents.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"

bool	CheckMoney::Check(const std::string file_name, const std::string event_id,
	FedMap *fed_map, FedMessages *messages, FedEvents *fed_events)
{
	if ((higher == "") && (equals == "") && (lower == ""))
	{
		std::ostringstream	buffer;
		buffer << event_id << " - Checkmoney - you haven't given me any events to call.";
		Tesla::Instance()->IssueError(file_name, buffer.str());
		return(false);
	}

	if ((value < 0) || (value > 1000))
	{
		std::ostringstream	buffer;
		buffer << event_id << " - Checkmoney - the value to test must be between 0 and 1000 inclusive.";
		Tesla::Instance()->IssueError(file_name, buffer.str());
		return(false);
	}

	if (!EventExists(file_name, event_id, higher, " - Checkmoney - higher event ", fed_events))
		return(false);
	if (!EventExists(file_name, event_id, equals, " - Checkmoney - equals event ", fed_events))
		return(false);
	if (!EventExists(file_name, event_id, lower, " - Checkmoney - lower event ", fed_events))
		return(false);

	return(true);
}


