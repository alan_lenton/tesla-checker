/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef STAR_H
#define STAR_H

#include <map>
#include <string>

class	FedEvents;
class	FedMap;
class	FedMessages;

typedef	std::map<std::string, FedEvents *, std::less<std::string> >	FedEventIndex;
typedef	std::map<std::string, FedMap *, std::less<std::string> >	MapIndex;
typedef	std::map<std::string, FedMessages *, std::less<std::string> >	MssgIndex;

class	Star
{
private:
	FedEventIndex	event_index;
	MapIndex			map_index;
	MssgIndex		mssg_index;

public:
	Star()	{	}
	~Star()	{ Clear(); }

	FedEvents	*FindEvents(const std::string& root);
	FedMap		*FindMap(const std::string& root);
	FedMessages	*FindMssg(const std::string& root);

	bool	OrbitExists(const std::string& title, const std::string& orbit);
	bool	SpaceMapContains(const std::string& planet_name);
	bool	TestEvents();
	bool	TestMap();
	bool	TestMessages();

	void	AddEvents(FedEvents *fed_events);
	void	AddMap(FedMap *fed_map);
	void	AddMessages(FedMessages *fed_mssg);
	void	Clear();
};

#endif
