/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "fedmssgs.h"

#include <sstream>

#include <cctype>
#include <cstdlib>

#include "message.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"

FedMessages::FedMessages(const std::string& f_name) : file_name(f_name)
{
	std::string	temp(f_name);
	unsigned		index = temp.find_last_of('.');
	temp = temp.substr(0, index);
	index = temp.find_last_of('.');
	file_root = temp.substr(index + 1);
}

FedMessages::~FedMessages()
{
	for (MessageIndex::iterator iter = index.begin(); iter != index.end(); ++iter)
		delete iter->second;
	index.clear();
}


void	FedMessages::AddMessage(Message *message)
{
	index[message->Id()] = message;
}

Message	*FedMessages::FindMessage(const std::string& which)
{
	MessageIndex::iterator	iter = index.find(which);
	if (iter != index.end())
		return(iter->second);
	else
		return(0);
}

bool	FedMessages::Test()
{
	Tesla::Instance()->Display("  ");
	std::ostringstream	buffer;
	buffer << "Testing " << file_name << " - " << Size() << " messages";
	Tesla::Instance()->Display(buffer);

	if (!TestIdFormat())
		return(false);

	Tesla::Instance()->Display("Tests complete");
	return(true);
}

bool	FedMessages::TestIdFormat()
{
	for (MessageIndex::iterator iter = index.begin(); iter != index.end(); ++iter)
	{
		std::ostringstream	buffer;
		std::string	mssg_id(iter->first);
		int			num_dots = 0;
		unsigned		len = mssg_id.length();
		for (int count = 0; count < len; ++count)
		{
			if (mssg_id[count] == '.')
			{
				if (++num_dots == 2)
				{
					if (std::isdigit(mssg_id[count + 1]) == 0)
					{
						buffer << mssg_id << " - Message names should be in the form 'text.text.number'!";
						Tesla::Instance()->IssueError(file_name, buffer.str());
						return(false);
					}
					else
						return(true);
				}
				else
					continue;
			}
			if (std::islower(mssg_id[count]) == 0)
			{
				buffer << mssg_id << " - Message names must be lower case!";
				Tesla::Instance()->IssueError(file_name, buffer.str());
				return(false);
			}
		}
	}
	return(true);
}



