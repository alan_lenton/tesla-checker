/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and /or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <map>
#include <string>


class	Message
{
private:
	std::string	id;
	std::string	text;

public:
	Message(const std::string& id_string) : id(id_string)	{		}
	~Message()															{		}

	const std::string&	Text()								{ return(text); }
	const std::string&	Id()									{ return(id); }
	void	Text(const std::string& txt)						{ text = txt; }
};

#endif

