/*-----------------------------------------------------------------------
Tesla - Windows Federation 2 Star System Checker
Copyright (c) 1985-2015 Alan Lenton

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation: either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY: without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You can find the full text of the GNU General Public Licence at
http://www.gnu.org/copyleft/gpl.html

Programming and design: 	Alan Lenton (email: alan@ibgames.com)
Home website:					www.ibgames.net
-----------------------------------------------------------------------*/

#include "fedmap.h"

#include <sstream>

#include <cctype>
#include <cstdlib>

#include "fedmssgs.h"
#include "location.h"
#include "locflags.h"
#include "misc.h"
#include "star.h"
#include "tesla.h"


FedMap::FedMap(const std::string& f_name) : file_name(f_name)
{
	std::string	temp(f_name);
	unsigned		index = temp.find_last_of('.');
	temp = temp.substr(0, index);
	index = temp.find_last_of('.');
	file_root = temp.substr(index + 1);
}

FedMap::~FedMap()
{
	for (LocIndex::iterator iter = loc_index.begin(); iter != loc_index.end(); iter++)
		delete iter->second;
	loc_index.clear();
}

void	FedMap::AddLocation(Location *loc)
{
	loc_index[loc->Number()] = loc;
}

int	FedMap::HasLink(int from, int dir, int to)
{
	LocIndex::iterator	iter = loc_index.find(from);
	if (iter == loc_index.end())
	{
		std::ostringstream	buffer;
		buffer << "location " << to << " has a link to a non-existant location";
		Tesla::Instance()->IssueError(title, buffer.str());
		return(Location::NO_LOC);
	}

	return(iter->second->HasLink(dir, to));
}

bool	FedMap::LocExists(int number)
{
	return(loc_index.find(number) != loc_index.end());
}

bool	FedMap::TestExchanges()
{
	std::ostringstream	buffer;
	int	total = 0;
	for (LocIndex::iterator iter = loc_index.begin(); iter != loc_index.end(); iter++)
	{
		if (iter->second->IsExchange())
		{
			buffer.str("");
			buffer << "Exchange found at " << title << "/" << iter->second->Number();
			buffer << " - " << iter->second->Name();
			Tesla::Instance()->Display(buffer);
			total++;
		}
	}

	if (file_name == "space.loc")
	{
		if (total != 0)
		{
			Tesla::Instance()->IssueError(title, "Drive in space exchanges are not allowed!");
			return(false);
		}
	}
	else
	{
		if (total == 0)
			Tesla::Instance()->IssueWarning(title, " has no exchange.");
		else
		{
			if (total > 1)
			{
				Tesla::Instance()->IssueError(title, "Only one exchange is allowed on each planet!");
				return(false);
			}
		}
	}
	return(true);
}

bool	FedMap::TestHyperSpaceLink()
{
	if (file_name != "space.loc")
		return(true);

	std::ostringstream	buffer;
	int	total_links = 0;
	for (LocIndex::iterator iter = loc_index.begin(); iter != loc_index.end(); iter++)
	{
		if (iter->second->TestFlag(Location::LINK))
		{
			buffer.str("");
			buffer << "Hyperspace link found at " << title << "/" << iter->second->Number();
			buffer << " - " << iter->second->Name();
			Tesla::Instance()->Display(buffer);
			total_links++;
		}
	}

	if (total_links == 1)
		return(true);

	if (total_links == 0)
		Tesla::Instance()->IssueError(title, " no hyperspace link found!");
	else
		Tesla::Instance()->IssueError(title, " mulpiple hyperspace links found!");

	return(false);
}

bool	FedMap::TestLaunchOrbit()
{
	if (file_name == "space.loc")	// It's a space map
	{
		if ((launch_loc != "") || (orbit_loc != ""))
		{
			Tesla::Instance()->IssueError(title,
				" launch and orbit locations should be empty for space map!");
			return(false);
		}
		return(true);
	}

	// else must be a planet map
	if ((launch_loc == "") || (orbit_loc == ""))
	{
		if (Tesla::Instance()->GetDirectory().find("/sol") != std::string::npos)
		{
			Tesla::Instance()->IssueWarning(title, "No orbit or launch locations...");
			return(true);
		}
		else
		{
			Tesla::Instance()->IssueError(title,
				" launch and orbit locations must be completed for non-space maps!");
			return(false);
		}
	}

	if (std::atoi(launch_loc.c_str()) <= 0)
	{
		Tesla::Instance()->IssueError(title,
			" launch location must be just a positive number!");
		return(false);
	}

	if (!LocExists(std::atoi(launch_loc.c_str())))
	{
		Tesla::Instance()->IssueError(title, " launch location doesn't exist!");
		return(false);
	}

	Location *orbit = loc_index.find(std::atoi(launch_loc.c_str()))->second;
	if(orbit->TestFlag(Location::FIGHTING))
	{
		Tesla::Instance()->IssueError(title, " launch location has a fighting flag set!");
		return(false);
	}

	return(NsTesla::star->OrbitExists(file_root, orbit_loc));
}

bool	FedMap::TestLocations()
{
	Tesla::Instance()->Display("  ");
	std::ostringstream	buffer;
	buffer << "Testing " << title << " - " << loc_index.size() << " locations...";
	Tesla::Instance()->Display(buffer);

	if (!TestExchanges())			return(false);
	if (!TestTitle())				return(false);
	if (!TestLaunchOrbit())		return(false);
	if (!TestHyperSpaceLink())	return(false);
	if (!TestNoExitMessages())	return(false);

	CheckEdges();	// reinitialise
	for (LocIndex::iterator iter = loc_index.begin(); iter != loc_index.end(); iter++)
	{

		iter->second->TestTextPresent(this);
		if (!iter->second->TestMovementTable(this))
			return(false);
		if (file_name == "space.loc")
		{
			if (!iter->second->SpaceFlagsOK(this))
				return(false);
		}
		else
		{
			if (!iter->second->PlanetFlagsOK(this))
				return(false);
		}
		if (!iter->second->OutSystemFlagsOK(this))
			return(false);
		CheckEdges(iter->second->Number());
	}

	Tesla::Instance()->Display("Tests complete");
	return(true);
}

bool	FedMap::TestTitle()
{
	if (title == "")
	{
		Tesla::Instance()->IssueError(file_name, "Map has no title!");
		return(false);
	}

	TestTitleNormalised();

	if (file_name == "space.loc")
	{
		std::string::size_type	index = title.find(" Space");
		if (index == std::string::npos)
		{
			Tesla::Instance()->IssueError(title,
				" space.loc map needs a title in form 'Xxxx Space', where 'Xxxx' is the name of the System.");
			return(false);
		}
	}
	else
	{
		if (NsTesla::star->SpaceMapContains(title + " "))
		{
			if (Tesla::Instance()->ContinueTests())
				Tesla::Instance()->IssueError(title,
				"space.loc map contains the planet name instead of the system name!");
			return(false);
		}
	}
	return(true);
}

bool	FedMap::TitleContains(const std::string& name)
{
	return(title.find(name) != std::string::npos);
}

bool	FedMap::TestTitleNormalised()
{
	if ((std::isalpha(title[0]) == 0) || (std::isupper(title[0]) == 0))
	{
		Tesla::Instance()->IssueError(file_name, "Map titles must start with a capital letter!");
		return(false);
	}

	unsigned	len = title.length();
	for (int count = 1; count < len; ++count)
	{
		if ((std::isalnum(title[count]) == 0) && (title[count] != ' '))
		{
			Tesla::Instance()->IssueError(file_name, "Map titles can only have letters and numbers!");
			return(false);
		}

		if ((title[count - 1] == ' ') && (std::isdigit(title[count]) == 0))
		{
			if ((std::isalpha(title[count]) == 0) || (std::isupper(title[count]) == 0))
			{
				Tesla::Instance()->IssueError(file_name,
					"Words in map titles must start with a capital letter, or be a number!");
				return(false);
			}
		}
		else
		{
			if ((title[count] != ' ') && (std::isalpha(title[count]) != 0))
			{
				if (std::islower(title[count]) == 0)
				{
					Tesla::Instance()->IssueError(file_name, "Only the start of a word in map titles can be a capital letter!");
					return(false);
				}
			}
		}
	}
	return(true);
}


void	FedMap::CheckEdges(int loc_no)
{
	static bool	top_edge = false;
	static bool	bottom_edge = false;
	static bool	left_edge = false;
	static bool	right_edge = false;

	if (loc_no == -1)	// reinitialise
	{
		top_edge = bottom_edge = false;
		left_edge = right_edge = false;
		return;
	}

	if (loc_no == 0)	// traditionally the store location
		return;

	if (!top_edge && (loc_no < WIDTH))	// top edge
	{
		Tesla::Instance()->IssueWarning(title, "Map cannot expand upwards.");
		top_edge = true;
	}
	if (!bottom_edge && (loc_no >= (WIDTH * (HEIGHT - 1))))	// bottom edge
	{
		Tesla::Instance()->IssueWarning(title, "Map cannot expand downwards.");
		bottom_edge = true;
	}
	if (!left_edge && ((loc_no % WIDTH) == 0))	// left edge
	{
		Tesla::Instance()->IssueWarning(title, "Map cannot expand to the left.");
		left_edge = true;
	}
	if (!right_edge && ((loc_no % WIDTH) == 63))	// right edge
	{
		Tesla::Instance()->IssueWarning(title, "Map cannot expand to the right.");
		right_edge = true;
	}
}


bool	FedMap::TestNoExitMessages()
{
	// NOTE: even if fed_mssg is zero, we still need to test, in case there
	// are messages referring to the non-existant file
	FedMessages	*fed_mssg = NsTesla::star->FindMssg(file_root);
	for (LocIndex::iterator iter = loc_index.begin(); iter != loc_index.end(); ++iter)
	{
		if (!iter->second->CheckNoExitFormat(this))
			return(false);
		if (!iter->second->TestNoExitMessage(this, fed_mssg))
			return(false);
	}
	return(true);
}